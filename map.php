<?php

require_once 'config.php';

try {
    $db = new PDO(
        'mysql:host=' . DB_HOST .';'
      . 'dbname=' . DB_NAME . ';'
      . 'port=' . DB_PORT . ';'
      . 'charset=' . DB_CSET,
        DB_USER,
        DB_PASS,
        array(
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        )
    );
} catch (PDOException $e) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 500 Internal Server Error', true, 500);
    echo '500 Internal Server Error';
    exit;
}

$points = 500;
if (array_key_exists('points', $_REQUEST)) {
    if (intval($_REQUEST['points']) >= 2 && intval($_REQUEST['points']) <= 5000) {
        $points = intval($_REQUEST['points']);
    }
}

$q = $db->prepare('CALL GetLastLocations(?)');
$q->execute(array($points));
$result = $q->fetchAll();
if (!array_key_exists(0, $result)) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 500 Internal Server Error', true, 500);
    echo '500 Internal Server Error';
    exit;
}

$path = '';
foreach ($result as $line) {
    $path .= sprintf('[%s, %s],', $line['Latitude'], $line['Longitude']);
}
$path = rtrim($path, ',');

?>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title></title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="//cdn.jsdelivr.net/normalize/3.0.2/normalize.min.css">

        <script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"></script>
    </head>
    <body style="background-color: #222;">

        <div id="map" style="position:absolute;top:0;right:0;bottom:0;left:0;"></div>

        <script src="//maps.google.com/maps/api/js?sensor=true"></script>
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//cdn.jsdelivr.net/gmaps/0.4.15/gmaps.min.js"></script>
        <script type="text/javascript">
            function chunk(arr, len) {
                var chunks = [];
                for (var i = 1; i <= (arr.length / len); ++i) {
                    chunks.push(arr.slice((i * len) - 1, (i * len) + len));
                }
                return chunks;
            }
            function componentToHex(c) {
                var hex = c.toString(16);
                return hex.length == 1 ? "0" + hex : hex;
            }
            function rgbToHex(r, g, b) {
                return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
            }
            function getGreenToRed(percent){
                r = percent < 50 ? 255 : Math.floor(255 - (percent * 2 - 100) * 255 / 100);
                g = percent > 50 ? 255 : Math.floor((percent * 2) * 255 / 100);
                return rgbToHex(r, g, 0);
            }
            var map;
            var path = [<?php echo $path; ?>];
            var path2 = chunk(path, path.length / 100);
            var bounds = [];
            var styles = [
                {
                    'featureType': 'all',
                    'elementType': 'labels.text.fill',
                    'stylers': [
                        {
                            'saturation': 36
                        },
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 40
                        }
                    ]
                },
                {
                    'featureType': 'all',
                    'elementType': 'labels.text.stroke',
                    'stylers': [
                        {
                            'visibility': 'on'
                        },
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 16
                        }
                    ]
                },
                {
                    'featureType': 'all',
                    'elementType': 'labels.icon',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'administrative',
                    'elementType': 'geometry.fill',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 20
                        }
                    ]
                },
                {
                    'featureType': 'administrative',
                    'elementType': 'geometry.stroke',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 17
                        },
                        {
                            'weight': 1.2
                        }
                    ]
                },
                {
                    'featureType': 'landscape',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 20
                        }
                    ]
                },
                {
                    'featureType': 'poi',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 21
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'geometry.fill',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 17
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'geometry.stroke',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 29
                        },
                        {
                            'weight': 0.2
                        }
                    ]
                },
                {
                    'featureType': 'road.arterial',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 18
                        }
                    ]
                },
                {
                    'featureType': 'road.local',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 16
                        }
                    ]
                },
                {
                    'featureType': 'transit',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 19
                        }
                    ]
                },
                {
                    'featureType': 'water',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#000000'
                        },
                        {
                            'lightness': 17
                        }
                    ]
                }
            ];
            $(document).ready(function() {
                map = new GMaps({
                    div: '#map',
                    lat: 60.228811,
                    lng: 25.001460,
                    zoom: 16,
                    backgroundColor: '#222'
                });

                map.addStyle({
                    styledMapName: 'Dark',
                    styles: styles,
                    mapTypeId: 'dark'
                });
                map.setStyle('dark');

                for (var i = 0; i < path2.length; ++i) {
                    map.drawPolyline({
                        path: path2[i],
                        strokeColor: getGreenToRed(i),
                        strokeOpacity: 0.5,
                        strokeWeight: 3
                    });
                }
            });
        </script>
    </body>
</html>