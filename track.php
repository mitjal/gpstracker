<?php

require_once 'config.php';

if (array_key_exists('pass', $_REQUEST) &&
    array_key_exists('lat', $_REQUEST) &&
    array_key_exists('lon', $_REQUEST)) {
    if ($_REQUEST['pass'] == PASS) {
        if (strlen($_REQUEST['lat']) && strlen($_REQUEST['lon'])) {
            try {
                $db = new PDO(
                    'mysql:host=' . DB_HOST .';'
                  . 'dbname=' . DB_NAME . ';'
                  . 'port=' . DB_PORT . ';'
                  . 'charset=' . DB_CSET,
                    DB_USER,
                    DB_PASS,
                    array(
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    )
                );
            } catch (PDOException $e) {
                header($_SERVER["SERVER_PROTOCOL"] . ' 500 Internal Server Error', true, 500);
                echo '500 Internal Server Error';
                exit;
            }
            $q = $db->prepare('CALL AddLocation(?, ?, ?)');
            $q->execute(array(
                $_REQUEST['lat'],
                $_REQUEST['lon'],
                $_SERVER['REMOTE_ADDR']
            ));
            $result = $q->fetchAll();
            if (array_key_exists(0, $result)) {
                header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK', true, 200);
                echo '200 OK';
                exit;
            }
        }
    }
}

header($_SERVER["SERVER_PROTOCOL"] . ' 400 Bad Request', true, 400);
echo '400 Bad Request';
exit;
