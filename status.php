<?php

require_once 'config.php';

try {
    $db = new PDO(
        'mysql:host=' . DB_HOST .';'
      . 'dbname=' . DB_NAME . ';'
      . 'port=' . DB_PORT . ';'
      . 'charset=' . DB_CSET,
        DB_USER,
        DB_PASS,
        array(
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        )
    );
} catch (PDOException $e) {
    header($_SERVER["SERVER_PROTOCOL"] . ' 500 Internal Server Error', true, 500);
    echo '500 Internal Server Error';
    exit;
}
$q = $db->prepare('CALL GetLastLocations(?)');
$q->execute(array(1));
$result = $q->fetchAll();
if (array_key_exists(0, $result)) {
    if (array_key_exists('Added', $result[0])) {
        header($_SERVER["SERVER_PROTOCOL"] . ' 200 OK', true, 200);
        echo 'Last tracked ' . $result[0]['Added'];
        exit;
    }
}

header($_SERVER["SERVER_PROTOCOL"] . ' 500 Internal Server Error', true, 500);
echo '500 Internal Server Error';
exit;
